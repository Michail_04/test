import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MainFormComponent } from './components/main-form/main-form.component';

const routes: Routes = [
  {path: '', component: MainFormComponent},
  {path: '**', component: MainFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
