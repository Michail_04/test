import { Component, OnInit } from '@angular/core';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { SharedDataService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class MainFormComponent implements OnInit {

  constructor(
    private sharedDataService: SharedDataService
  ) { }

  public firstStepControl = this.sharedDataService.getFirstStepControl();

  ngOnInit() {
  }
}
