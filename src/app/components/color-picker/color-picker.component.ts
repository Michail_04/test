import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {

  public colors = [
    {color: '#a9d08e', selected: false},
    {color: '#ffd966', selected: false},
    {color: '#f4b084', selected: false},
  ];

  @Output() colorEmiter = new EventEmitter<string>();

  ngOnInit() {
  }

  public pickColor(item: {color: string, selected: boolean}): void {
    this.colors.forEach(color => color.selected = color.color == item.color)
    this.colorEmiter.emit(item.color);
  }

}
