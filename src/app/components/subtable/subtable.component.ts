import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ISubtable, IQuestion } from 'src/app/services/fetch-data.service';
import { FormGroup } from '@angular/forms';
import { MatStep } from '@angular/material/stepper';

@Component({
  selector: 'app-subtable',
  templateUrl: './subtable.component.html',
  styleUrls: ['./subtable.component.scss']
})
export class SubtableComponent implements OnInit{

  @Input() subtable: ISubtable;
  @Input() title: string;
  @Input() tableIndex: number;
  @Input() tableLength: number;
  @Input() control: FormGroup;
  
  @Output() onNext = new EventEmitter<any>();
  @Output() onLast = new EventEmitter<any>();

  public disabled = true;

  constructor() { }

  ngOnInit() {
  }

  public setColor(color: string, question: IQuestion) {
    question.color = color;
    this.disabled = !this.subtable.questions.every(val => !!val.color);
  }

  public next(): void {
    if(!this.disabled) {
      this.control.get('status').setValue(true);
      this.tableLength !== this.tableIndex ?  this.onNext.emit() : this.onLast.emit();
    }
  }
}
