import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild, AfterViewInit, Query, QueryList, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { FetchDataService, ITable } from 'src/app/services/fetch-data.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { SharedDataService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-main-table',
  templateUrl: './main-table.component.html',
  styleUrls: ['./main-table.component.scss']
})
export class MainTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatHorizontalStepper) stepper: MatHorizontalStepper; 

  private tableSubscription = new Subscription();

  public controls: {[key:string]: FormGroup}

  public tables: ITable[] = [];

  public tableLength: number;

  @Output() onNext = new EventEmitter<any>();

  constructor(
    private fetchDataService: FetchDataService,
    private fb: FormBuilder,
    private sds: SharedDataService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.tableSubscription.add(this.fetchDataService.getTable().subscribe((data: ITable[]) => {
      this.tables = data;
      this.sds.setTables(this.tables);
      this.setControls();
    }));
  }

  ngAfterViewInit() {
    this.stepper.steps.changes.subscribe((list: QueryList<any>) => {
      this.tableLength = list.length;
      this.cdr.detectChanges();
    })
  }

  ngOnDestroy() {
    this.tableSubscription.unsubscribe();
  }

  private setControls() {
    this.controls = this.tables.reduce((akk, table) => {
      table.subtables.forEach(subtables => {
        akk = {...akk, [table.id + subtables.subtableId]: this.fb.group({status: [false, Validators.requiredTrue]})}
      });
      return akk;
    }, {});
  }

  public next(): void {
     this.onNext.emit(); 
  }
}
