import { Component, OnInit, Input, OnDestroy, NgZone, ChangeDetectorRef } from '@angular/core';
import { ITable } from 'src/app/services/fetch-data.service';
import { SharedDataService, IInfo } from 'src/app/services/shared.service';
import { Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas'; 

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();

  public tables: ITable[] = [];

  public aboutCompanyForm: FormGroup;
  public contactDetailsForm: FormGroup;

  public load = false;

  public info: IInfo;

  constructor(
    private sds: SharedDataService,
    private zone: NgZone,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.subscription.add(this.sds.getTables().subscribe((tables: ITable[]) => this.tables = tables));
    this.sds.getInfo().subscribe((info: IInfo) => this.info = info);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  saveToPdf() {
    this.load = true;
    this.zone.runOutsideAngular(() => {
      const data = document.getElementById('table');
      html2canvas(data).then(canvas => {
        const contentDataURL = canvas.toDataURL('image/png')  
        let pdf = new jspdf('p', 'mm', 'a4');
        var position = 0;  
        pdf.addImage(contentDataURL, 'PNG', 0, position,  210, 297)  
        pdf.save('MYPdf.pdf');
        this.load = false;
        this.cdr.detectChanges();
        });
    });
  }

  reload() {
    document.location.reload(true);
  }
}
