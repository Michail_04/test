import { Component, OnInit, Output, EventEmitter, OnDestroy} from '@angular/core';
import { SharedDataService } from 'src/app/services/shared.service';
import { FormGroup, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { IProces, FetchDataService } from 'src/app/services/fetch-data.service';
import { Subscription, combineLatest } from 'rxjs';
import { ErrorStateMatcher } from '@angular/material/core';
import { withLatestFrom } from 'rxjs/operators';

class MyErrorStateMatcher  implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid);
  }
}

@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.scss']
})
export class DataFormComponent implements OnInit, OnDestroy {

  public aboutCompanynForm: FormGroup;
  public contactDetails: FormGroup;

  public industrys: IProces[] = [];
  public myErrorStateMatcher = new MyErrorStateMatcher();
  public disabled = true;

  private mainFormSubsription = new Subscription(); 
  @Output() onNext = new EventEmitter<any>();

  constructor(
    private sharedDataService: SharedDataService,
    private fetchDataService: FetchDataService,
  ) { }

  ngOnInit() {
    this.mainFormSubsription.add(this.fetchDataService.getIndustryNames().subscribe((data: IProces[]) => this.industrys = data));

    this.aboutCompanynForm = this.sharedDataService.getAboutCompanyForm();
    this.contactDetails = this.sharedDataService.getContactDetailsForm();

    combineLatest(this.aboutCompanynForm.statusChanges, this.contactDetails.statusChanges).subscribe((val: string[]) => {
      this.disabled = !(val[0] === "VALID" && val[1] === "VALID");
    })

  
  }

  ngOnDestroy() {
    this.mainFormSubsription.unsubscribe();
  }

  public next(): void {
    if(this.aboutCompanynForm.valid && this.contactDetails.valid){
      this.sharedDataService.getFirstStepControl().get('status').setValue(true);
      const temp = this.sharedDataService.getAboutCompanyForm();
      this.sharedDataService.setInfo({
        companyName: temp.get('companyName').value,
        industry: temp.get('industry').value
      });
      this.onNext.emit();
    }
  }
}
