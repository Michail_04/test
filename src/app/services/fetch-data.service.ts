import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

export interface IData {
    proceses: IProces[];
    table: ITable[];
    loading: boolean;
}

export interface IProces {
    id: number;
    name: string;
    selected: boolean;
}

export interface ITable {
    id: number;
    title: string;
    subtables: ISubtable[];
}

export interface ISubtable {
    index: number;
    subtableId: number;
    subtitle: string;
    questions: IQuestion[];
}

export interface IQuestion {
    id: number;
    text: string;
    answer: string;
    color: string;
}

@Injectable()
export class FetchDataService {

    private endpoint: string;

    private INIT_STATE: IData = {
        proceses: [],
        table: [],
        loading: true
    }

    private data = new BehaviorSubject<IData>(this.INIT_STATE);

    constructor(
        private http: HttpClient
    ){
        this.endpoint = environment.production ?  './assets/' : '../../assets/';
    }

    public fetchData() {
        this.data.next({...this.INIT_STATE, loading: true});
        this.http.get(`${this.endpoint}data.json`).subscribe((data: IData) => this.data.next({...data, loading: false}));
    }

    private getData(): Observable<IData> {
        return this.data.asObservable();
    }

    public getIndustryNames(): Observable<IProces[]> {
        return this.getData().pipe(
            map((data: IData) => data.proceses)
        )
    }

    public getTable(): Observable<ITable[]> {
        return this.getData().pipe(
            map((data: IData) => data.table)
        )
    }
}