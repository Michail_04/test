import { Injectable } from '@angular/core';
import { FormBuilder, Validators, AbstractControl, FormGroup, FormControl } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { ITable } from './fetch-data.service';

export interface IInfo {
    companyName: string;
    industry: string;
  }

@Injectable()
export class SharedDataService {

    constructor(private fb: FormBuilder) {}

    private tables = new BehaviorSubject<ITable[]>([]);
    private info = new BehaviorSubject<IInfo>(null);

    private contactDetailsForm = this.fb.group({
        'email': ['',  Validators.compose([Validators.required, Validators.email]) ],
        'fio': ['', Validators.required]
    });

    private aboutCompany = this.fb.group({
        'companyName': ['', Validators.required],
        'industry': ['', Validators.required],
    });

    private firstStepControl = this.fb.group({
        status: [false, Validators.requiredTrue]
    });

    getAboutCompanyForm(): FormGroup {
        return this.aboutCompany;
    }

    getContactDetailsForm(): FormGroup {
        return this.contactDetailsForm;
    }

    getFirstStepControl(): FormGroup {
        return this.firstStepControl;
    }

    setTables(data: ITable[]) {
        this.tables.next(data);
    }

    getTables(): Observable<ITable[]> {
        return this.tables.asObservable();
    }

    setInfo(info: IInfo): void {
        this.info.next(info);
    }

    getInfo(): Observable<IInfo> {
        return this.info.asObservable();
    }
}