import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainFormComponent } from './components/main-form/main-form.component';
import { SharedModul } from './shared.module';
import { HttpClientModule } from '@angular/common/http';
import { FetchDataService } from './services/fetch-data.service';
import { DataFormComponent } from './components/data-form/data-form.component';
import { MainTableComponent } from './components/main-table/main-table.component';
import { ResultComponent } from './components/result/result.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedDataService } from './services/shared.service';
import { SubtableComponent } from './components/subtable/subtable.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
@NgModule({
  declarations: [
    AppComponent,
    MainFormComponent,
    DataFormComponent,
    MainTableComponent,
    ResultComponent,
    SubtableComponent,
    ColorPickerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModul
  ],
  providers: [
    FetchDataService,
    SharedDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
