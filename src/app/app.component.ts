import { Component, OnInit } from '@angular/core';
import { FetchDataService } from './services/fetch-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(private fetchDataServer: FetchDataService){}

  ngOnInit(){
    this.fetchDataServer.fetchData();
  }
}
